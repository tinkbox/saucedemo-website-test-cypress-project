/// <reference types ="cypress" />

describe('Sauce Demo Website Login', ()=>{
    beforeEach('Test to open website and login with valid credetials',()=>{
        cy.visit('/')
        cy.fixture('userData').then(userData =>{
        const username = userData.valid_username
        const password = userData.valid_password
        cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
    })
    })
    it('Verify Burger menu is clickable and "About" link opens the correct page from the burger menu',()=>{
        cy.get('#react-burger-menu-btn').click()
        cy.get('#about_sidebar_link').click()
        cy.title().should('eq','Sauce Labs: Cross Browser Testing, Selenium Testing & Mobile Testing')
    })
    it('Verify user can logout from the Burger menu',()=>{
        cy.get('#react-burger-menu-btn').click()
        cy.get('#logout_sidebar_link').click()
        cy.url().should('eq','https://www.saucedemo.com/')
    })
    it('Verify "Reset App State" and "All Items" link are clickable from the burger menu',()=>{
        cy.get('#react-burger-menu-btn').click()
        cy.get('#inventory_sidebar_link').click()
        cy.get('#reset_sidebar_link').click()
    })
})