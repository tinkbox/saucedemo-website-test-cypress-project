/// <reference types ="cypress" />
describe('Test suite for SauceDemo website login page',()=>{
    beforeEach('Open and navigate to saucedemo website landing page before running each test script',()=>{
        cy.visit('/')
    })
    it('Verify login page',()=>{
        cy.title().should('eq','Swag Labs')
        cy.url().should('eq', 'https://www.saucedemo.com/')
    })
    it('Verify user login for valid user', ()=> {
        cy.fixture('userData').then(userData =>{
            const username = userData.valid_username
            const password = userData.valid_password
            cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
        })
    })
    it('Verify user login for invalid user',()=>{
        cy.fixture('userData').then(userData =>{
            const username = userData.invalid_username
            const password = userData.invalid_password
        cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
        cy.get('[data-test="error"]').should('contain.text', 'Epic sadface')
        })
        })
    it('Verify user login with blank username and password',()=>{
        cy.get('#user-name').type(" ")
        cy.get('[id="password"]').type(" ")
        cy.get('#login-button').click()
        cy.get('[data-test="error"]').should('contain.text', 'Epic sadface')
        })
})  