/// <reference types="cypress" />

import 'cypress-each'

describe('Sauce Demo Website Login', ()=>{
    beforeEach('Test to open website and login with valid credetials',()=>{
        cy.visit('/')
        cy.fixture('userData').then(userData =>{
        const username = userData.valid_username
        const password = userData.valid_password
        cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
    })
    })
    it('Adding items to cart using item name and verify the cart is updated', function (){
        cy.contains('.inventory_item_description', 'Sauce Labs Backpack')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Bike Light')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Fleece Jacket')
        .within(function() {
           cy.get('.btn').click()
        })
        cy.get('.shopping_cart_badge').should('contain.text', '3')
    })
    it('Verify added items can be removed from cart',()=>{
        cy.contains('.inventory_item_description', 'Sauce Labs Backpack')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Bike Light')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Fleece Jacket')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.get('.btn:contains("Remove")').eq(1).click()
    })
    it('Verify filter is clickable',()=>{
          cy.get('select').select('Price (high to low)')
    })
  
    const data = [
        ['.social_twitter > a','have.attr','href','https://twitter.com/saucelabs'], 
        ['.social_facebook > a','have.attr','href','https://www.facebook.com/saucelabs'],
        ['.social_linkedin > a','have.attr','href','https://www.linkedin.com/company/sauce-labs/'],
    ]
    it.each(data)('Social media link test - %s', (selector, assertion, method1, value2) => {
        cy.get(selector).should(assertion, method1, value2).click()
    })

   it('Verify copywrite information for Sauce Labs appears at the button of the website',()=>{
      cy.get('.footer_copy:contains("All Rights Reserved. Terms of Service")').should('exist')
   })

//    it('Verify facebook link',()=>{
//     cy.get('.social_facebook > a').should('have.attr','href','https://www.facebook.com/saucelabs').click()
//    })
})
