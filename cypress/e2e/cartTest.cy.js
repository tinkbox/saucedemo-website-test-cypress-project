/// <reference types="cypress" />

describe('Test suite for SauceDemo website login page', ()=>{
    beforeEach('Test to open website and login with valid credetials',()=>{
        cy.visit('/')
        cy.fixture('userData').then(userData =>{
            const username = userData.valid_username
            const password = userData.valid_password
            cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
        })
    })
    it('Verify "Continue Shopping" button works',()=>{
        cy.get('.shopping_cart_link').click()
        cy.get('[data-test="continue-shopping"]').click()
    })

    it('Verify number of items in the list',()=>{
        cy.fixture('userData').then(userData => {
            const list = userData.cart_item_list
            cy.get('.inventory_item_name').its('length').should('eq', list)
        })
    })

    it('Verify items added to cart and removed from cart and Checkout works',()=>{
        cy.contains('.inventory_item_description', 'Sauce Labs Backpack')
        .within(()=>{
           cy.get('.btn').click()
           cy.get('.inventory_item_price').should('contain','$').as('backpackPrice')
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Bike Light')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Fleece Jacket')
        .within(()=> {
           cy.get('.btn').click()
        })
        cy.get('.shopping_cart_badge').should('contain.text', '3')        
       cy.get('.shopping_cart_link').click()
       cy.contains('.cart_item', 'Sauce Labs Fleece Jacket')
       .within(function() {
          cy.get('.btn').click()
       })
       cy.get('.shopping_cart_badge').should('contain.text', '2')
       cy.get('[data-test="checkout"]').click()
       cy.url().should('eq','https://www.saucedemo.com/checkout-step-one.html')
    })
})