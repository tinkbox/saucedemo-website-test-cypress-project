/// <reference types="cypress" />

let item_list = 2;
let getTax
let getSubTotal
let getTotal

describe('Test suite for SauceDemo website login page', ()=>{

    beforeEach('Test to open website and login with valid credetials',()=>{
        cy.visit('/')
        cy.fixture('userData').then(userData =>{
            const username = userData.valid_username
            const password = userData.valid_password
            cy.get('#user-name').type(username)
        cy.get('input#password').type(password)
        cy.get('#login-button').click()
        })
        
        cy.get('.inventory_item_name')
           
        cy.contains('.inventory_item_description', 'Sauce Labs Backpack')
        .within(()=>{
           cy.get('.btn').click()
        })
        cy.contains('.inventory_item_description', 'Sauce Labs Bike Light')
        .within(()=>{
           cy.get('.btn').click()
         })
        cy.contains('.inventory_item_description', 'Sauce Labs Fleece Jacket')
        .within(()=> {
           cy.get('.btn').click()
        })
        
        cy.get('.shopping_cart_link').click()
        cy.contains('.cart_item', 'Sauce Labs Fleece Jacket')
        .within(function() {
           cy.get('.btn').click()
        })
        cy.get('[data-test="checkout"]').click()
    })
    it('Verify use can cancel on the checkout page',()=>{
        cy.get('[data-test="cancel"]').click()
    })

    it('Verify the checkout page accepts user delivery data, "Continue" button works, use can cancel checkout, displays payment information',()=>{
        cy.fixture('userData').then(userData => {
            const firstName = userData.firstName
            const lastName = userData.lastName
            const zipCode = userData.zipCode
            cy.get('[data-test="firstName"]').type(firstName)
            cy.get('[data-test="lastName"]').type(lastName)
            cy.get('[data-test="postalCode"]').type(zipCode)
            cy.get('[data-test="continue"]').click()
        })
        cy.fixture('userData').then(userData => {
            const list = userData.checkout_item_list
            cy.get('.inventory_item_name').its('length').should('eq',list)
        })

        cy.contains('.summary_info > :nth-child(2)', 'SauceCard #31337')
        cy.contains('.summary_info > :nth-child(4)', 'Free Pony Express Delivery!')
        
        cy.contains('.summary_subtotal_label','Item total: $39.98')
        cy.get('.summary_subtotal_label').then(($txtSubTotal)=>{
            getSubTotal = $txtSubTotal.text()
        })

        cy.contains('.summary_tax_label','Tax: $3.20')
        cy.get('.summary_tax_label').then(($txtTax) => {
          getTax = $txtTax.text()
        })

       cy.contains('.summary_total_label','Total: $43.18')
       cy.get('.summary_total_label').then(($txtTotal)=>{
            getTotal =$txtTotal.text()
        })

        cy.get('[data-test="finish"]',).click()
        cy.get('.complete-header').should('contain.text', 'Thank you for your order!')
        cy.get('.complete-text').should('include.text', 'Your order')
        cy.get('[data-test="back-to-products"]').click()
        cy.url().should('eq','https://www.saucedemo.com/inventory.html')
    })
})